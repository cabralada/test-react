import React, { Component } from 'react';

import styled from 'styled-components';

const MasterList = styled.li`
    background: #F6F6F6;
    margin: 0;

    @media only screen and (min-width: 768px) {
        margin: 2rem 0 0 0;
    }

    padding: 1.5rem 2rem;

    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;

    position: relative;

    .card {
        &--title,
        &--by {
            display: block;
        }

        &--by {
            font-size: .8rem;
            color: #ccc;
        }

        &--title {
            font-size: 3rem
        }

        &:after {
            content: '';
            background: #94D600;
            display: block;
            height: 5px;
            width: 100vw;
            position: absolute;
            top: -5px;
            left: 0;

            @media only screen and (min-width: 768px) {
                width: 100px;
            }
        }
    }

    .btn {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;

        display: block;
        margin-top: .6rem;
        padding: .6rem;

        text-decoration: none;
        color: black;

        cursor: pointer;

        border: 1px solid black;

        @media only screen and (min-width: 768px) {
            display: inline-block;
        }

        &:hover {
            @media only screen and (min-width: 768px) {
                color: white;
                background: black;
            }
        }
    }
`;

class ItemNews extends Component {
    constructor(props) {
        super(props);
        console.log(props)
        this.state = {
            by : props.dataItem.by,
            id : props.dataItem.id,
            title: props.dataItem.title,
            url: props.dataItem.url
        }
    }

    componentDidMount() {
        console.log('Lista item montado')
    }


    render() {
        return (
            <MasterList>
                <div className="card">
                    <span className="card--by">{this.state.by}</span>
                    <span className="card--title">{this.state.title}</span>
                    <a className="btn" target="_blank" href={this.state.url}>Link</a>
                </div>
            </MasterList>
        );
      }
}

export default ItemNews;