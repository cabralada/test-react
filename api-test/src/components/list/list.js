import React, { Component } from 'react';
import axios from 'axios';
import styled from 'styled-components';

import ItemNews from './list-item/item'


const MasterList = styled.ul`
    max-width: 1000px;
    margin: 0 auto;
`;

class ListNews extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: this.props.news,
            itemData: []
        }

    }

    collectInfo(data) {
        data.map ((item, index) => {
            let url = `https://hacker-news.firebaseio.com/v0/item/${item}.json`;
            axios.get(url)
                .then(info => {
                    this.setState({
                        itemData:  [...this.state.itemData, info.data]
                    });
                });
        })
    }

    componentDidMount() {
        this.collectInfo(this.state.list)
    }

    render() {
        if (this.state.itemData.length < this.props.news.length) {
            return <div>Loading itens</div>
        }

        return (
            <MasterList>
                {
                    this.props.news.map((item, index) => {
                        return <ItemNews key={index} dataItem={this.state.itemData[index]} id={index} />;
                    })
                }
            </MasterList>
        );
        }
  }


export default ListNews;