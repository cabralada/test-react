import React, { Component } from 'react';
import axios from 'axios';

import './App.css';

import ListNews from './../list/list';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      topStores: []
    }
  }

  componentDidMount() {
    axios.get(`https://hacker-news.firebaseio.com/v0/topstories.json`)
      .then(res => {
        this.setState({
          topStores: res.data
        })
      });
  }


  render() {

    if (this.state.topStores.length === 0) {
        return <div>Loading app ...</div>
    }

    return (
      <div className="App">
        <ListNews news={this.state.topStores} />
      </div>
    );
  }
}

export default App;
