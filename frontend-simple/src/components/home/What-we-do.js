import React, { Component } from 'react';

import ballon from './../../assets/speech-bubble.svg';

import { 
    baseStyle_borderBox,
    media
} from './../../base-style';
import styled from 'styled-components';

const ComponentMaster = styled.section`
    position: relative;

    &:before,
    &:after{
        content: '';
        position: absolute;
        background: #ffde17;
        height: 100%;
        left: 0;
        width: 50%;
        top: 0
    }

    &:after {
        left: auto;
        right: 0;
        background: #ffc222;
    }

    .container {
        position: relative;
        z-index: 1;
        padding: 0;

        ${ media.desktop`
            padding: 0 10px;
        ` }
    }

    .what-we-do {
        &--title {
            font-size: 45px;
            text-align: center;
            font-weight: 700;
            text-transform: uppercase;
            color: #4a4a4a;
            position: absolute;
            z-index: 5;
            width: 100%;
            top: 140px;
            text-shadow: 0 1px rgba(255,255,255,.5);

            ${ media.tablet`
                font-size: 65px;
            ` }
        }

        &--ballons {
            padding: 250px 0 50px;

            ${ media.tablet`
                padding: 0;
            ` }

            li {
                width: 100%;
                display: inline-block;
                text-align: center;
                overflow: hidden;
                z-index: 3;
                margin-bottom: 50px;

                ${ media.tablet`
                    width: 25%;
                    height: 540px;
                    margin-bottom: 0;
                ` }

                position: relative;

                &:nth-child(2) {
                    &:before {
                        content: '';
                        display: block;
                        width: 100%;
                        height: 300%;
                        top: -100%;
                        position: absolute;
                        z-index: 0;

                        ${ media.tablet`
                            background: #ffd41c;
                        ` }
                    }
                }

                &:nth-child(3) {
                    ${ media.tablet`
                        background: #ffcb1f;
                    ` }
                }

                strong {
                    font-size: 18px;
                    color: white;
    
                    background: url(${ballon}) no-repeat;
                    height: 210px;
                    background-position: center center;
                    display: block;

                    position: relative;
                    z-index: 2;
                    padding-top: 70px;
                    top: calc(50% - 25px);
                    text-transform: uppercase;
                    font-weight: 600;
                    line-height: 1.2rem;

                    ${baseStyle_borderBox};
                }
            }

            &:after{
                content: '';
                width: 215px;
                height: 215px;
                display: block;
                bottom: -45px;
                position: absolute;
                left: calc(50% - 106px);
                transform: rotate(-45deg);

                background: rgba(255,222,23,1);
                background: -moz-linear-gradient(-45deg, rgba(255,222,23,1) 0%, rgba(255,222,23,1) 49%, rgba(255,194,34,1) 50%, rgba(255,194,34,1) 100%);
                background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(255,222,23,1)), color-stop(49%, rgba(255,222,23,1)), color-stop(50%, rgba(255,194,34,1)), color-stop(100%, rgba(255,194,34,1)));
                background: -webkit-linear-gradient(-45deg, rgba(255,222,23,1) 0%, rgba(255,222,23,1) 49%, rgba(255,194,34,1) 50%, rgba(255,194,34,1) 100%);
                background: -o-linear-gradient(-45deg, rgba(255,222,23,1) 0%, rgba(255,222,23,1) 49%, rgba(255,194,34,1) 50%, rgba(255,194,34,1) 100%);
                background: -ms-linear-gradient(-45deg, rgba(255,222,23,1) 0%, rgba(255,222,23,1) 49%, rgba(255,194,34,1) 50%, rgba(255,194,34,1) 100%);
                background: linear-gradient(135deg, rgba(255,222,23,1) 0%, rgba(255,222,23,1) 49%, rgba(255,194,34,1) 50%, rgba(255,194,34,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FFDE17', endColorstr='#ffcb1f', GradientType=1 );

                ${ media.tablet`
                    background: rgba(255,213,28,1);
                    background: -moz-linear-gradient(-45deg, rgba(255,213,28,1) 0%, rgba(255,213,28,1) 49%, rgba(255,203,31,1) 50%, rgba(255,203,31,1) 100%);
                    background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(255,213,28,1)), color-stop(49%, rgba(255,213,28,1)), color-stop(50%, rgba(255,203,31,1)), color-stop(100%, rgba(255,203,31,1)));
                    background: -webkit-linear-gradient(-45deg, rgba(255,213,28,1) 0%, rgba(255,213,28,1) 49%, rgba(255,203,31,1) 50%, rgba(255,203,31,1) 100%);
                    background: -o-linear-gradient(-45deg, rgba(255,213,28,1) 0%, rgba(255,213,28,1) 49%, rgba(255,203,31,1) 50%, rgba(255,203,31,1) 100%);
                    background: -ms-linear-gradient(-45deg, rgba(255,213,28,1) 0%, rgba(255,213,28,1) 49%, rgba(255,203,31,1) 50%, rgba(255,203,31,1) 100%);
                    background: linear-gradient(135deg, rgba(255,213,28,1) 0%, rgba(255,213,28,1) 49%, rgba(255,203,31,1) 50%, rgba(255,203,31,1) 100%);
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffd51c', endColorstr='#ffcb1f', GradientType=1 );
                ` }


            }
        }
    }

`;
class WhatWeDo extends Component {
    render() {
        return (
        <ComponentMaster>
            <div className='container'>
                <h2 className='what-we-do--title'>
                    What we do
                </h2>

                <ul className="what-we-do--ballons">
                    <li><strong>Web <br/> development</strong></li>
                    <li><strong>PHP <br/> Consulting</strong></li>
                    <li><strong>PHP <br/> Training</strong></li>
                    <li><strong>Application <br/> Support</strong></li>
                </ul>
            </div>
        </ComponentMaster>
        );
    }
}

export default WhatWeDo;
