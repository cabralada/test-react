import React, { Component } from 'react';

import ProductsList from './Product-list'

import customProducts from './../../assets/product-details.json';

import {media} from './../../base-style';

import styled from 'styled-components';

const ComponentMaster = styled.section`
    padding: 50px 0 70px;
    background: white;
    box-shadow: 0 3px 3px 0 rgba(0,0,0,.1);

    position: relative;
    z-index: 3;

    &:before,
    &:after {
        content: '';
        background: white;
        width: 215px;
        height: 215px;
        display: block;
        bottom: -45px;
        position: absolute;
        left: calc(50% - 106px);
        transform: rotate(-45deg);
        z-index: 0;
        box-shadow: 0 0 3px 3px rgba(0,0,0,.1);
    }

    &:before {
        background: white;
        width: 100%;
        height: 215px;
        bottom: -55px;
        left: 0;
        transform: rotate(0deg);
        z-index: 1;
        bottom: 0;
        box-shadow: 0 0 0 0;
    }

    .products {
        position: relative;
        z-index: 10;

        &--title {
            font-size: 45px;
            text-align: center;
            text-transform: uppercase;
            color: #ffc222;
            font-weight: 700;
            margin-bottom: 40px;

            ${ media.tablet`
                font-size: 65px;
            ` }
        }

        &--items {
            margin-top: -70px;
            text-align: center;

            ${ media.desktop`
                text-align: left;
            ` }
        }

        &--list-item {
            display: inline-block;
            margin: 70px 5px 0;
            text-align: center;
            width: 100%;

            ${ media.tablet`
                width: auto
            ` }

            &--main-img {
                height: auto;
                width: 85vw;
                border: 1px solid #f2f2f2;

                ${ media.tablet`
                    width: 42vw;
                ` }

                ${ media.desktop`
                    width: auto;
                    height: 330px;
                ` }
            }

            &--name-item {
                display: block;
                margin: 10px 0;
                font-weight: 600;
                font-size: 18px;
            }

            &--selector {
                margin-top: 10px;
                &--color {
                    border: 1px solid #cccccc;
                    margin: 0 3px;
                    position: relative;
                    cursor: pointer;
    
                    width: 23px;
                    height: 23px;
                    display: inline-block;
    
                    img {
                        position: absolute;
                        top: 1px;
                        left: 1px;
                    }

                    &:hover {
                        border: 1px solid #666;
                    }

                    &.active-color {
                        border: 1px solid #000000;
                    }
                }
            }

            &--new-price,
            &--old-price {
                font-size: 1rem;
            }

            &--old-price {
                margin-right: 7px
                text-decoration: line-through;
            }

            &--new-price {
                margin-left: 7px
            }

        }
    }

`;

class Products extends Component {
    render() {
        return (
        <ComponentMaster>
            <div className="container products">
                <h2 className="products--title">Products</h2>

                <ul className="products--items">
                    {
                        customProducts.products.map((index, item) => {
                            return <ProductsList key={item} item={index} />
                        })
                    }
                </ul>
            </div>
        </ComponentMaster>
        );
    }
}

export default Products;
