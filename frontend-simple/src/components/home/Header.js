import React, { Component } from 'react';

import { 
    iconFacebook,
    iconTwitter,
    iconLinkedin,
    iconPhone,
    iconGooglePlus,
    LogoInviqa,
    baseStyle_borderBox,
    baseStyle_borderRadius,
    baseStyle_gradient,
    media
} from './../../base-style';

import styled from 'styled-components';

const Logo = () => {
    return LogoInviqa();
}

const LogoFacebook = () => {
    return iconFacebook();
}

const LogoTwitter = () => {
    return iconTwitter();
}

const LogoLinkedin = () => {
    return iconLinkedin();
}

const LogoPhone = () => {
    return iconPhone();
}

const LogoGooglePlus = () => {
    return iconGooglePlus();
}


const ComponentMaster = styled.header`

    ${baseStyle_gradient('rgba(255,213,28,1)', 'rgba(253,183,0,1)', '#ffd51c', '#fdb700')}

    .header {
        &--super {
            background: white;

            .container {
                overflow: hidden;
                font-size: 11px
            }

            &--go-left{
                display: none;

                ${ media.tablet`
                    display: block;
                    float: left;
                ` }

                .btn {
                    color: #82827f;
                    display: block;
                    padding: 8px 10px;
                    margin: 0;
                    text-transform: uppercase;
                    font-weight: 600;
                    text-decoration: none;

                    ${baseStyle_borderRadius('1000rem')}
                    ${baseStyle_gradient('rgba(232,232,232)', 'rgba(241,241,241)', '#ffffff', '#f4f4f4')}

                    box-shadow: inset 0 2px 2px 0 #d5d5d5;

                    ${ media.tablet`
                        margin: 15px 6px 15px 0;
                        display: inline-block;
                    ` }

                    ${ media.desktop`
                        margin: 15px 10px 15px 0;
                    ` }

                    &:hover {
                        ${ media.desktop`
                            box-shadow: inset 0 -2px 0 0 #d5d5d5;
                        ` }
                    }
                }
            }

            &--go-right{
                ${ media.tablet`
                    float: right;
                ` }

                li {
                    margin-left: 10px;
                    position: relative;
                    ${baseStyle_borderBox()}

                    &:last-child {
                        margin-left: 35px;
                    }

                    a {
                        display: inline-block;
                        position: relative;
                        color: black;

                        &.icon {
                            width: 25px;
                            height: 25px;
                            top: 17px;

                            svg {
                                width: 25px;
                                height: 25px;
                                position: absolute;
                            }
                        }
                    }

                    &.only-icon {
                        width: 25px;
                        height: 60px;
                        opacity: .1;

                        &:hover {
                            opacity: .5
                        }
                    }

                    &.with-icon {
                        float: right;
                        position: relative;
                        padding-left: 15px;
                        top: 15px;

                        ${ media.tablet`
                            float: none;
                            top: 9px;
                        ` }

                        ${ media.desktop`
                            padding-left: 35px;
                        ` }

                        span {
                            padding: 5px 0 6px;
                            display: inline-block;
                        }

                        svg {
                            width: 25px;
                            height: 25px;
                            position: absolute;
                            top: 0;
                            left: -35px;
                            opacity: .1;
                        }

                        &:hover {
                            svg {
                                opacity: .5
                            }
                        }
                    }
                }
            }

            ul {
                li {
                    display: inline-block;
                    font-size: 14px;
                    font-weight: 600;
                }
            }

            &:after {
                content: '';
                display: block;
                height: 20px;
                width: 100%;
                background: green;
                position: absolute;
                ${baseStyle_gradient('rgba(0,0,0,1)', 'rgba(255,255,255,0)', '#000000', '#ffffff')}
                opacity: .1;
            }
        }

        &--intro {
            max-width: 470px;
            text-align: center;
            margin: 0 auto;
            padding: 78px 0;

            text-shadow: 0 1px rgba(255,255,255, .5);

            &--title {
                margin-bottom: 15px;
                text-transform: uppercase;
                font-size: 25px;
                font-weight: 700;
                letter-space: -1px;

                ${ media.tablet`
                    font-size: 35px;
                ` }
            }

            &--txt {
                line-height: 1.6rem;
                font-size: 18px;

                margin: 0 20px;

                ${ media.tablet`
                    margin: 0;
                ` }
            }
        }
  }
`;

class Header extends Component {
    render() {
        return (
        <ComponentMaster>
            <section className='header--super'>
                <div className="container">
                    <ul className='header--super--go-left'>
                        <li><a href="/" className='btn' alt="">Case Study</a></li>
                        <li><a href="/" className='btn' alt="">What we do</a></li>
                        <li><a href="/" className='btn' alt="">Resource</a></li>
                        <li><a href="/" className='btn' alt="">Contact us</a></li>
                    </ul>
                    <ul className='header--super--go-right'>
                        <li className="only-icon"><a className='icon' href="/" alt=""><LogoFacebook /></a></li>
                        <li className="only-icon"><a className='icon' href="/" alt=""><LogoTwitter /></a></li>
                        <li className="only-icon"><a className='icon' href="/" alt=""><LogoLinkedin /></a></li>
                        <li className="only-icon"><a className='icon' href="/" alt=""><LogoGooglePlus /></a></li>
                        <li className="with-icon"><a href="/" alt=""><LogoPhone /> <span>020 3179 9555</span></a></li>
                    </ul>
                </div>
            </section>

            <section className='header--intro'>
            <div>

                <Logo />

                <h2 className='header--intro--title'>We have a new name</h2>
                <p className='header--intro--txt'>
                    Inviqa, formally know as Ibuildings are starting the
                    new year with a fresh new innovate look. We are a brunch
                    of intelligent people who always pride the
                    quality in our work.
                </p>
            </div>
            </section>
        </ComponentMaster>
        );
    }
}

export default Header;
