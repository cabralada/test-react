import React, { Component } from 'react';
import './Home.css';

import Header from './Header';
import Products from './Products';
import Resources from './Resources';
import WhatWeDo from './What-we-do';

// All elements of the design should be included
// ✓ The page layout should be responsive
// ✓ Make use of a CSS preprocessor such as Sass or LESS
// ✓ We have made use of the Google font ‘Open Sans’ throughout the page and it should be
// linked as an external font.
// ✓ The social icons and telephone icon should be included as an icon font (the font has been
// provided as part of the assets).
// ✓ The company logo and speech bubbles have been provided and should remain in vector
// format.
// ✓ In the ‘Products’ section, the swatches for the products should be clickable to switch
// between different shirt colours.
// ✓ The page should be tested against the following browsers: Chrome (latest), Firefox (latest),
// Internet Explorer (11).
// ✓ All files (including images) required by your page should be submitted, make sure the links
// to them are relative so we can open your page in a browser without any problems.
// ✓ Use the included JSON file to manage and render the product attributes.

class Home extends Component {
    render() {
        return (
          <section>
            <Header />
            <Products />
            <WhatWeDo />
            <Resources />
          </section>
        );
    }
}

export default Home;

