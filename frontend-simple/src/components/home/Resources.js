import React, { Component } from 'react';

import bg from './../../assets/bg-gray.png';
import techPortal from './../../assets/techportal.jpg'

import styled from 'styled-components';

import { 
    baseStyle_borderBox,
    baseStyle_rowFlexBox,
    media
} from './../../base-style';

const ComponentMaster = styled.section`
    padding: 140px 0 70px;
    background: url(${bg});

    .resources {

        &.container {
            color: #ffd41c;
            ${baseStyle_rowFlexBox()}
        }

        &--title {
            font-size: 45px;
            text-align: center;
            text-transform: uppercase;
            color: #ffc222;
            font-weight: 700;
            margin-bottom: 40px;
            position: relative;
            padding-bottom: 40px;

            ${ media.tablet`
                font-size: 65px;
            ` }

            &:before,
            &:after{
                content: '';
                display: block;
                height: 1px;
                width: 100%;
                position: absolute;
                bottom: 0;
                background: red
            }

            &:after {
                bottom: 0px;
                background: white;
                opacity: .1
            }

            &:before {
                bottom: 2px;
                background: black;
                opacity: 1
            }

            &--section {
                text-transform: uppercase;
                font-weight: 600;
                color: white;
                padding-bottom: 15px;
                margin-bottom: 15px;
                position: relative;

                &:before,
                &:after{
                    content: '';
                    display: block;
                    height: 1px;
                    width: 100%;
                    position: absolute;
                    bottom: 0;
                    background: red
                }

                &:after {
                    bottom: 0px;
                    background: white;
                    opacity: .1
                }

                &:before {
                    bottom: 2px;
                    background: black;
                    opacity: 1
                }
            }
        }

        &--whitepaper {
            padding-bottom: 60px;
            position: relative;
            line-height: 1.2rem;
            margin: 0 2rem;

            ${ media.tablet`
                width: 32%;
                margin: 0;
            ` }

            li {
                margin-top: 10px;
                &:first-child {
                    margin-top: 0;
                }
            }

        }

        &--newsletter {
            padding-bottom: 60px;
            width: 100%;
            position: relative;
            line-height: 1.2rem;
            margin: 50px 2rem;

            ${ media.tablet`
                width: calc(28% - 46px);
                margin: 0 23px;
            ` }

            a {
                color: #ffd41c;

                &:hover {
                    text-decoration: none;
                }
            }
        }

        &--techportal {
            padding-bottom: 60px;
            width: 100%;
            position: relative;
            line-height: 1.2rem;
            overflow: hidden;
            margin: 0 2rem;

            ${ media.tablet`
                width: 40%;
                margin: 0;
            ` }
            

            span {
                ${baseStyle_borderBox()};

                ${ media.tablet`
                    float: left;
                    width: 40%;
                ` }

                img {
                    margin-bottom: 5px;
                }

                &.txt {
                    margin-bottom: 25px;
                    display: block;

                    ${ media.tablet`
                        padding-right: 30px;
                        width: 60%;
                        margin-bottom: 0;
                    ` }
                }

                a {
                    color: #ffd41c;
                    display: block;
                }
            }
        }

        &--call-to-action {
            position: absolute;
            padding-top: 15px;
            width: 100%;
            bottom: 0;

            a {
                color: white;
                text-decoration: none;
                strong {
                    color: #fabc07
                }

                &:hover {
                    strong {
                        text-decoration: underline;
                    }
                }
            }

            &:before,
            &:after{
                content: '';
                display: block;
                height: 1px;
                width: 100%;
                position: absolute;
                top: 0;
                background: red
            }

            &:after {
                top: 0px;
                background: white;
                opacity: .1
            }

            &:before {
                top: 2px;
                background: black;
                opacity: 1
            }
        }
    }
`;
class Resources extends Component {
    render() {
        return (
        <ComponentMaster>
            <h2 className="resources--title">Resources</h2>

            <div className="resources container">
                <ul className="resources--whitepaper">
                    <li className="resources--title--section">White paper</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit..</li>
                    <li>Fusce tortor arcu, accumsan in turpis et, efficitur porttitor ligula.</li>
                    <li>Morbi ac gravida diam.</li>
                    <li className="resources--call-to-action">
                        <a href="/">More <strong>white papers</strong></a>
                    </li>
                </ul>

                <div className="resources--newsletter">
                    <h3 className="resources--title--section">Resources</h3>
                    <p>
                        <a href="/" alt="test">Click here</a> to enter your email address and receive our Monthly Enewsletter
                    </p>
                    <div className="resources--call-to-action">
                        <a href="/">More <strong>resources</strong></a>
                    </div>
                </div>

                <div className="resources--techportal">
                    <h3 className="resources--title--section">Inviqa Techportal</h3>
                    <span className="txt">The Inviqa techPortal is an information base for everything related to PHP – from developers, to developers. </span>
                    <span>
                        <img src={techPortal} alt="Techportal" />
                        <a href="/">Visit our techPortal</a>
                    </span>
                    <div className="resources--call-to-action">
                        <a href="/">View <strong>the latest vacancies</strong></a>
                    </div>
                </div>
            </div>


        </ComponentMaster>
        );
    }
}

export default Resources;
