import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Notification from './Notification'

class App extends Component {
  constructor() {
    super();
    this.state = {
      notification: {
          message: 'Hello World',
          type: 'error'
      }
    }
  }

  render() {
    return (
      <div className="App">
        <Notification 
          type = {this.state.notification.type}
          txt = {this.state.notification.message}
        />
      </div>
    );
  }
}

export default App;
