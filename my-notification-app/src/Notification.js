import React, { Component } from 'react';
import './Notification.css';

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type : this.props.type
    }
  }

  checkType() {
    if (this.state.type === 'success') {
      return 'alert alert-success'
    }

    if (this.state.type === 'message') {
      return 'alert alert-info'
    }

    if (this.state.type === 'caution') {
      return 'alert alert-warning'
    }

    if (this.state.type === 'error') {
      return 'alert alert-danger'
    }

  }

  render() {
    return (
      <div className={this.checkType()}>
        {this.props.txt}
      </div>
    );
  }
}

export default Notification;
